<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMacToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->macAddress('mac');
            $table->ipAddress('ip');
            $table->string('father_name');
            $table->string('mobile');
            $table->integer('is_delete');
            $table->integer('verified');
            $table->string('facebook_url');
            $table->string('twitter_url');
            $table->string('linkedin_url');
            $table->string('instagram_url');
            $table->string('website');
            $table->string('city');
            $table->string('about');
            $table->string('image');
            $table->string('age');
            $table->string('citizenship');
            $table->string('code');
            $table->string('job');
            $table->string('award_won');
            $table->string('permanent_address');
            $table->string('cureent_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
