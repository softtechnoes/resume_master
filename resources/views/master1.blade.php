<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img//apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{ asset('assets/img//favicon.png')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    @yield('title')
  </title>
  <meta name="theme-color" content="#ff6600" />

  {!! SEOMeta::generate() !!}
  {!! OpenGraph::generate() !!}
  {!! Twitter::generate() !!}
  {!! JsonLd::generate() !!}
  <!-- OR -->
  {!! SEO::generate() !!}

  <!-- MINIFIED -->
  {!! SEO::generate(true) !!}

  <!-- LUMEN -->
  {!! app('seotools')->generate() !!}
  
 
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!-- Extra details for Live View on GitHub Pages -->
  <!-- Canonical SEO -->
  <link rel="canonical" href="https://www.softtechnoes.xyz" />
  <!--  Social tags      -->
  {{--  <meta name="keywords" content="Rajendra Santaushi, Rajendra Santaushi Website, Soft Technoes, Softtechnoes, Rajendra Santaushi blog, Soft technoes blog, resume builder, html resume template, resume template, free html builder, create resume online, free resume, resu.me, make resume free, attractive resume, build resume quick, quick resume builder">
  <meta name="description" content="Create powerful resumes that capture the attention of hiring managers with our fast and easy to use Resume Builder. Choose a template and build your resume in minutes.">
  <!-- Schema.org markup for Google+ -->
  <meta itemprop="name" content="Resume Builder by Soft Technoes">
  <meta itemprop="description" content="Create powerful resumes that capture the attention of hiring managers with our fast and easy to use Resume Builder. Choose a template and build your resume in minutes.">
  <meta itemprop="image" content="../../raw.githubusercontent.com/creativetimofficial/public-assets/master/paper-kit-2-pro-html/opt_pk2p_thumbnail.jpg">
  <!-- Twitter Card data -->
  <meta name="twitter:card" content="product">
  <meta name="twitter:site" content="@santaushi">
  <meta name="twitter:title" content="Resume Builder | Soft Technoes">
  <meta name="twitter:description" content="Create powerful resumes that capture the attention of hiring managers with our fast and easy to use Resume Builder. Choose a template and build your resume in minutes.">
  <meta name="twitter:creator" content="@santaushi">
  <meta name="twitter:image" content="{{ asset('assets/img/antoine-barres.jpg')}}">

  
  <!-- Open Graph data -->
  <meta property="fb:app_id" content="655968634437471">
  <meta property="og:title" content="Resume Builder | Soft Technoes" />
  <meta property="og:type" content="article" />
  <meta property="og:url" content="index.html" />
  <meta property="og:image" content="{{ asset('assets/img/antoine-barres.jpg')}}" />
  <meta property="og:description" content="Create powerful resumes that capture the attention of hiring managers with our fast and easy to use Resume Builder. Choose a template and build your resume in minutes." />
  <meta property="og:site_name" content="Soft Technoes" />

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' /> --}}
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="{{ asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{ asset('assets/css/paper-kit.css?v=2.2.0')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{ asset('assets/demo/demo.css')}}" rel="stylesheet" />
  <link href="{{ asset('assets/css/style.css')}}" rel="stylesheet"/>

  {{-- Notifications --}}
  
  <link href="{{asset('plugins/notify/css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
  {{-- Scanner --}}
  {{-- <script src="{{ asset('assets/js/instascan.min.js')}}" type="text/javascript"></script> --}}
  <script src="{{ asset('assets/js/scanner.js')}}" type="text/javascript"></script>
  <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MTCWJ7K');</script>
<!-- End Google Tag Manager -->
</head>

<body class="index-page sidebar-collapse">
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MTCWJ7K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg fixed-top" >
    <div class="container-fluid">
      <div class="navbar-translate">
        <a class="navbar-brand" href="/" rel="tooltip" title="AutoCv" data-placement="bottom">
          {{-- Resume --}}
          <img src="{{asset('logo1.png')}}" alt="Auto CV" height="43" style="height:43px; margin: -32px 0 -27px 0;">
        </a>
        <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-bar bar1"></span>
          <span class="navbar-toggler-bar bar2"></span>
          <span class="navbar-toggler-bar bar3"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse justify-content-end" id="navigation">
        <ul class="navbar-nav">
          @if (Auth::user())
            <li class="nav-item">
              <a class="nav-link" rel="tooltip" title="View My Resume" data-placement="bottom" href="/view-resume">
                <i class="fa fa-eye"></i>
                <p class="d-lg-none">View My Resume</p>
              </a>
            </li>
          @endif
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="https://twitter.com/santaushi" target="_blank">
              <i class="fa fa-twitter"></i>
              <p class="d-lg-none">Twitter</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="https://www.facebook.com/wanderingblackpanther" target="_blank">
              <i class="fa fa-facebook-square"></i>
              <p class="d-lg-none">Facebook</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="https://www.instagram.com/wandering_black_panther" target="_blank">
              <i class="fa fa-instagram"></i>
              <p class="d-lg-none">Instagram</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="Star on GitHub" data-placement="bottom" href="https://www.github.com/softtechnoes" target="_blank">
              <i class="fa fa-github"></i>
              <p class="d-lg-none">GitHub</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="QR Code Scanner" data-placement="bottom" href="qr-scanner">
              <i class="fa fa-qrcode"></i>
              <p class="d-lg-none">QR Code Generator</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="QR Code Scanner" data-placement="bottom" href="scanner">
              <i class="fa fa-mobile"></i>
              <p class="d-lg-none">QR Code Scanner</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" rel="tooltip" title="Notes" data-placement="bottom" href="notes" target="_blank">
              <i class="fa fa-sticky-note"></i>
              <p class="d-lg-none">Notes</p>
            </a>
          </li>
          @if (Auth::user())
            <li class="nav-item dropdown">
              <a class="btn btn-just-icon btn-warning  " data-toggle="dropdown">
                <i class="nc-icon nc-sound-wave"></i>
              </a>
              <ul class="dropdown-menu dropdown-menu-right dropdown-notification">
                <li class="no-notification">
                  You're all clear!
                </li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a class="btn btn-just-icon btn-danger  " data-toggle="dropdown">
                <i class="nc-icon nc-email-85"></i>
              </a>
              <ul class="dropdown-menu dropdown-menu-right dropdown-wide dropdown-notification">
                <li class="dropdown-header">
                  You have 7 unread notifications
                </li>
                <li>
                  <ul class="dropdown-notification-list scroll-area">
                    <a href="#paper-kit" class="notification-item">
                      <div class="notification-text">
                        <span class="badge badge-pill badge-success"><i class="nc-icon nc-chat-33"></i></span>
                        <span class="message">
                          <b>Patrick</b> mentioned you in a comment.</span>
                        <br>
                        <span class="time">20min ago</span>
                        <button class="btn btn-just-icon read-notification btn-round">
                          <i class="nc-icon nc-check-2"></i>
                        </button>
                      </div>
                    </a>
                    <a href="#paper-kit" class="notification-item">
                      <div class="notification-text">
                        <span class="badge badge-pill badge-info"><i class="nc-icon nc-alert-circle-i"></i></span>
                        <span class="message">Our privacy policy changed!</span>
                        <br>
                        <span class="time">1day ago</span>
                      </div>
                    </a>
                    <a href="#paper-kit" class="notification-item">
                      <div class="notification-text">
                        <span class="badge badge-pill badge-warning"><i class="nc-icon nc-ambulance"></i></span>
                        <span class="message">Please confirm your email address.</span>
                        <br>
                        <span class="time">2days ago</span>
                      </div>
                    </a>
                    <a href="#paper-kit" class="notification-item">
                      <div class="notification-text">
                        <span class="badge badge-pill badge-primary"><i class="nc-icon nc-paper"></i></span>
                        <span class="message">Have you thought about marketing?</span>
                        <br>
                        <span class="time">3days ago</span>
                      </div>
                    </a>
                  </ul>
                </li>
                <!--      end scroll area -->
                <li class="dropdown-footer">
                  <ul class="dropdown-footer-menu">
                    <li>
                      <a href="#paper-kit">Mark all as read</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li class="nav-item dropdown">
              <a href="#paper-kit" class="nav-link navbar-brand" data-toggle="dropdown" width="30" height="30">
                <div class="profile-photo-small">
                  @if(Auth::user()->image=='#')
                  <img src="{{asset("images/user/user_male.png")}}" class="img-circle img-responsive img-no-padding" alt="{!! Auth::user()->name !!}">
                  @else
                  <img src="{{asset( Auth::user()->image )}}" class="img-circle img-responsive img-no-padding" alt="{!! Auth::user()->name !!}">
                  @endif
                </div>
              </a>
              <ul class="dropdown-menu dropdown-menu-right dropdown-danger">
                <div class="dropdown-header">Action</div>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="/home"><i class="fa fa-user"></i> Profile</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="/edit-profile"><i class="fa fa-gear"></i> Settings</a>
                <div class="dropdown-divider"></div>
                
                <a class="dropdown-item" href="/logout">
                  <i class="fa fa-sign-out"></i> Logout
                </a>
              </ul>
            </li>
          @else
            <a class="btn btn-round btn-danger" href="/login">
              <i class="fa fa-sign-in"></i> Login
            </a>
          @endif
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar -->
  @yield('hero')

    
    <div class="main">
      @yield('content')
      
    </div>
    <footer class="footer footer-white ">
      <div class="container-fluid">
        <div class="row">
          <nav class="footer-nav">
            <ul>
              <li>
                <a href="#" target="_blank">Team</a>
              </li>
              <li>
                <a href="https://www.topstack.xyz" target="_blank">Blog</a>
              </li>
              <li>
                <a href="#" target="_blank">Licenses</a>
              </li>
            </ul>
          </nav>
          <div class="credits ml-auto">
            <span class="copyright">
              ©
              <script>
                document.write(new Date().getFullYear())
              </script>, made with <i class="fa fa-heart heart"></i> by Soft Technoes
            </span>
          </div>
        </div>
      </div>
    </footer>
    <!--   Core JS Files   -->
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    {{-- <script src="{{ asset('assets/js/core/jquery.min.js')}}" type="text/javascript"></script> --}}
    <script src="{{ asset('assets/js/core/popper.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/core/bootstrap.min.js')}}" type="text/javascript"></script>
    <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
    <script src="{{ asset('assets/js/plugins/bootstrap-switch.js')}}"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{ asset('assets/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
    <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
    <script src="{{ asset('assets/js/plugins/moment.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <!-- Control Center for Paper Kit: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('assets/js/paper-kit.js?v=2.2.0')}}" type="text/javascript"></script>
    <!--  Google Maps Plugin    -->
    {{-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> --}}
    <script src="{{asset('plugins/notify/js/jquery.growl.js')}}" type="text/javascript"></script>
    <script>
      $(document).ready(function() {

        if ($("#datetimepicker").length != 0) {
          $('#datetimepicker').datetimepicker({
            icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fa fa-trash',
              close: 'fa fa-remove'
            }
          });
        }

        function scrollToDownload() {

          if ($('.section-download').length != 0) {
            $("html, body").animate({
              scrollTop: $('.section-download').offset().top
            }, 1000);
          }
        }
      });
    </script>
    @stack('scripts')
    @stack('js')
</body>

</html>
