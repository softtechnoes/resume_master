<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE>{{$user->name}} Resume Output</TITLE>
<META name="generator" content="BCL easyConverter SDK 5.0.210">
<STYLE type="text/css">

body {margin-top: 0px;margin-left: 0px;}

#page_1 {position:relative; overflow: hidden;margin: 56px 0px 92px 60px;padding: 0px;border: none;width: 733px;}
#page_1 #id1_1 {float:left;border:none;margin: 0px 0px 0px 33px;padding: 0px;border:none;width: 529px;overflow: hidden;}
#page_1 #id1_2 {float:left;border:none;margin: 2px 0px 0px 0px;padding: 0px;border:none;width: 171px;overflow: hidden;}

#page_1 #p1dimg1 {position:absolute;top:4px;left:0px;z-index:-1;width:543px;height:968px;}
#page_1 #p1dimg1 #p1img1 {width:543px;height:968px;}

#page_1 #p1inl_img1 {position:relative;width:9px;height:11px;}
#page_1 #p1inl_img2 {position:relative;width:15px;height:15px;}



#page_2 {position:relative; overflow: hidden;margin: 56px 0px 862px 62px;padding: 0px;border: none;width: 731px;height: 205px;}

#page_2 #p2dimg1 {position:absolute;top:10px;left:0px;z-index:-1;width:57px;height:195px;}
#page_2 #p2dimg1 #p2img1 {width:57px;height:195px;}




.ft0{font: bold 26px 'helvetica';color: #212121;line-height: 30px;}
.ft1{font: bold 13px 'helvetica';color: #2196f3;line-height: 16px;}
.ft2{font: bold 11px 'helvetica';color: #9e9e9e;line-height: 14px;}
.ft3{font: bold 22px 'helvetica';color: #212121;line-height: 26px;}
.ft4{font: bold 12px 'helvetica';color: #424242;line-height: 19px;}
.ft5{font: bold 15px 'helvetica';color: #212121;line-height: 18px;}
.ft6{font: bold 12px 'helvetica';color: #424242;line-height: 18px;}
.ft7{font: bold 12px 'helvetica';color: #424242;line-height: 17px;}
.ft8{font: bold 12px 'helvetica';color: #424242;line-height: 15px;}
.ft9{font: bold 13px 'helvetica';color: #212121;line-height: 16px;}
.ft10{font: bold 11px 'helvetica';color: #212121;line-height: 17px;}
.ft11{font: bold 10px 'helvetica';color: #2196f3;line-height: 19px;}
.ft12{font: bold 10px 'helvetica';color: #212121;line-height: 19px;}
.ft13{font: bold 11px 'helvetica';color: #212121;line-height: 19px;}
.ft14{font: bold 11px 'helvetica';color: #212121;line-height: 14px;}
.ft15{font: bold 11px 'helvetica';color: #212121;line-height: 16px;}
.ft16{font: bold 10px 'helvetica';color: #212121;line-height: 18px;}

.p0{text-align: left;padding-left: 136px;margin-top: 0px;margin-bottom: 0px;}
.p1{text-align: left;padding-left: 136px;margin-top: 11px;margin-bottom: 0px;}
.p2{text-align: left;padding-left: 136px;margin-top: 15px;margin-bottom: 0px;}
.p3{text-align: left;margin-top: 27px;margin-bottom: 0px;}
.p4{text-align: left;padding-left: 1px;padding-right: 47px;margin-top: 17px;margin-bottom: 0px;}
.p5{text-align: left;margin-top: 12px;margin-bottom: 0px;}
.p6{text-align: left;padding-left: 1px;margin-top: 14px;margin-bottom: 0px;}
.p7{text-align: left;padding-left: 1px;margin-top: 4px;margin-bottom: 0px;}
.p8{text-align: left;padding-left: 1px;padding-right: 71px;margin-top: 12px;margin-bottom: 0px;}
.p9{text-align: left;padding-left: 34px;padding-right: 54px;margin-top: 0px;margin-bottom: 0px;}
.p10{text-align: left;padding-left: 34px;padding-right: 67px;margin-top: 0px;margin-bottom: 0px;}
.p11{text-align: left;padding-left: 34px;padding-right: 78px;margin-top: 1px;margin-bottom: 0px;}
.p12{text-align: left;padding-left: 1px;margin-top: 12px;margin-bottom: 0px;}
.p13{text-align: justify;padding-left: 1px;padding-right: 69px;margin-top: 11px;margin-bottom: 0px;}
.p14{text-align: left;padding-left: 34px;padding-right: 52px;margin-top: 0px;margin-bottom: 0px;}
.p15{text-align: left;padding-left: 34px;padding-right: 68px;margin-top: 0px;margin-bottom: 0px;}
.p16{text-align: left;padding-left: 1px;margin-top: 15px;margin-bottom: 0px;}
.p17{text-align: left;padding-left: 1px;margin-top: 5px;margin-bottom: 0px;}
.p18{text-align: left;padding-left: 1px;padding-right: 61px;margin-top: 11px;margin-bottom: 0px;}
.p19{text-align: left;padding-left: 34px;margin-top: 1px;margin-bottom: 0px;}
.p20{text-align: left;padding-left: 34px;padding-right: 67px;margin-top: 2px;margin-bottom: 0px;}
.p21{text-align: left;padding-left: 34px;margin-top: 0px;margin-bottom: 0px;}
.p22{text-align: left;padding-left: 34px;padding-right: 52px;margin-top: 2px;margin-bottom: 0px;}
.p23{text-align: left;margin-top: 0px;margin-bottom: 0px;}
.p24{text-align: left;padding-right: 66px;margin-top: 15px;margin-bottom: 0px;}
.p25{text-align: left;padding-right: 77px;margin-top: 0px;margin-bottom: 0px;}
.p26{text-align: left;margin-top: 15px;margin-bottom: 0px;}
.p27{text-align: left;padding-right: 114px;margin-top: 6px;margin-bottom: 0px;}
.p28{text-align: left;margin-top: 7px;margin-bottom: 0px;}
.p29{text-align: left;margin-top: 22px;margin-bottom: 0px;}
.p30{text-align: left;margin-top: 6px;margin-bottom: 0px;}
.p31{text-align: left;padding-left: 1px;margin-top: 22px;margin-bottom: 0px;}
.p32{text-align: left;padding-right: 71px;margin-top: 14px;margin-bottom: 0px;}
.p33{text-align: left;padding-right: 82px;margin-top: 0px;margin-bottom: 0px;}
.p34{text-align: left;padding-right: 68px;margin-top: 0px;margin-bottom: 0px;}
.p35{text-align: left;padding-right: 45px;margin-top: 4px;margin-bottom: 0px;}
.p36{text-align: left;padding-left: 31px;margin-top: 0px;margin-bottom: 0px;}
.p37{text-align: left;padding-left: 32px;margin-top: 15px;margin-bottom: 0px;}
.p38{text-align: left;padding-left: 32px;margin-top: 4px;margin-bottom: 0px;}
.p39{text-align: left;padding-left: 65px;margin-top: 12px;margin-bottom: 0px;}
.p40{text-align: left;padding-left: 31px;margin-top: 20px;margin-bottom: 0px;}
.p41{text-align: left;padding-left: 32px;margin-top: 14px;margin-bottom: 0px;}

.round_img {
  border-radius: 50%;
  height: 100px;
  margin-left:28px;
}


</STYLE>
</HEAD>

<BODY>
<DIV id="page_1">
<DIV id="p1dimg1">
<IMG src="{{asset($user->image)}}" class="round_img">
</DIV>


<DIV>
<DIV id="id1_1">
<P class="p0 ft0">{{$user->name}} </P>
<P class="p1 ft1">{{$user->job}}</P>
<P class="p2 ft2"><IMG src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAALCAYAAACtWacbAAAARUlEQVQYlY2QwQoAMAhCc/T/v+xOgbVRdTNUXoGkTXNGh5m5CgCpliRSUzXozlVEsoYQ4L+mCK7AW9MDrjxtkxrTEZuPX1ptJA1YS5FFAAAAAElFTkSuQmCC" id="p1inl_img1"> {{$user->cureent_address}}</P>
<P class="p3 ft3">Proﬁle</P>
<P class="p4 ft4">{{$user->about}} </P>
{{-- <p class="p4 ft4">Example: Energetic Web Designer with 3 years experience creating and maintaining functional, attractive, and responsive websites for travel companies. Clear understanding of modern technologies and best design practices. Experienced with WordPress and Drupal. Proven track record of raising UX scores and customer retention.</p> --}}
<P class="p5 ft3">Employment History</P>
<P class="p6 ft5">Web Designer at Expedia Group, New York</P>
<P class="p7 ft1">January 2017 – May 2018</P>
<P class="p8 ft4">Expedia Group is a global travel company with websites which are primarily travel fare aggregators. As the Web Designer, my core activities included:</P>
<P class="p9 ft6">Planning site designs, functionality and navigation, along with audience funnels and data capture points.</P>
<P class="p10 ft7">Building wireframes & prototypes which were then turned into functional and responsive digital products.</P>
<P class="p11 ft4">Reviewing UX with multiple teams and making necessary edits to accommodate technical or business concerns. Raised UX scores by 38%. Handling all composition, color, illustration, typography, and branding for projects.</P>
<P class="p12 ft5">Web Designer at FarePortal, New York</P>
<P class="p7 ft1">February 2016 – December 2016</P>
<P class="p13 ft4">FarePortal is a travel technology company where the ﬂagship product CheapOair receives over 100 million visitors annually. As the Web Designer, my core activities included:</P>
<P class="p14 ft6">Designing, building, and maintaining marketing email creative using <NOBR>data-driven</NOBR> responsive templates.</P>
<P class="p15 ft6">Providing expertise on industry standards, best practices, and proper coding techniques to achieve correct rendering in all email environments. Performing quality assurance and troubleshooting code rendering across multiple desktop and mobile devices. Improved customer retention by 17%. Creating landing pages using WordPress CMS.</P>
<P class="p16 ft5">Web Designer at The Points Guy, New York</P>
<P class="p17 ft1">March 2015 – November 2015</P>
<P class="p18 ft4">The Points Guy is a site devoted to helping over 5 million monthly readers stay up to date on travel news. As the Web Designer, my core activities included:</P>
<P class="p19 ft8">Creating homepage assets for both desktop & mobile experiences.</P>
<P class="p20 ft6">Developing site content and graphics in partnership with writers and creative director. Spearheaded 4 projects simultaneously.</P>
<P class="p21 ft8">Designing images, audio enhancements, icons, and banners.</P>
<P class="p22 ft4">Presenting concepts and ideas consistent with company branding guidelines to the creative team.</P>
</DIV>
<DIV id="id1_2">
<P class="p23 ft9">Details</P>
<P class="p24 ft10">9 Wall St, New York, 10005, USA</P>
<P class="p25 ft12"><NOBR>890-555-0401</NOBR> <SPAN class="ft11">email@email.com</SPAN></P>
<P class="p26 ft2">DATE / PLACE OF BIRTH</P>
<P class="p27 ft13">1995/20/03 New York</P>
<P class="p26 ft2">NATIONALITY</P>
<P class="p28 ft14">USA</P>
<P class="p29 ft2">DRIVING LICENSE</P>
<P class="p30 ft14">Full</P>
<P class="p31 ft9"><IMG src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAAUElEQVQokbWTSwpAMQgDTXn3v/J0V4Rnaz84O8GYiCjAbmnXSjP7fCEpjQFo9ANbomjAMrZ3iXja+SfO3OqcIwBFieqdZ9TdOaP5Safo5as6tacnBwrXELUAAAAASUVORK5CYII=" id="p1inl_img2"> Skills</P>
<P class="p32 ft10">WordPress, Drupal, Joomla</P>
<P class="p33 ft10">HTML5, CSS, JS, jQuery</P>
<P class="p34 ft15">Adobe Photoshop & Illustrator</P>
<P class="p23 ft14">Sketch</P>
<P class="p35 ft16">Time management <NOBR>Deadline-driven</NOBR> Eﬀective communicator Team player Energetic and inventive</P>
</DIV>
</DIV>
</DIV>
<DIV id="page_2">
<DIV id="p2dimg1">
<IMG src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADCADkDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDf+K3xR8S+Ed1ppnhye0jk+RdWu1WSMk7wPLCEru+UON5zgHMeKw/hr8aPEeqXtvp2u6VJqVu9xDbvqlrFsNu0rlUMoA2YLFVGNnAP3jXeePfBninXtRGo+HvFP2HbafZjplzGXtLjJYO0incpyj4wY2+6PqKfgj4feI9Om8zxXrVpdWkSKltpWnReTaxskyTpLtQRrvDqc/JyDyxHFAHplFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAEN3I0VlPIhwyxswPoQKmqvf/wDIOuv+uL/yNWKACiiigAooooAKKKKAK9//AMg66/64v/I1Yqvf/wDIOuv+uL/yNWKACiiigAooooAKKKKAK9//AMg66/64v/I1Yqvf/wDIOuv+uL/yNWKACiiigAooooAKKKKAK9//AMg66/64v/I1Yqvf/wDIOuv+uL/yNWKACiiigAooooAKKwfEXh2bXLnS7i31e70+SwuPOBgwQ4IwQQeM4yATkYZgQQa3qAGyRrLG0bjKsCpHqDTqKKACiiigAooooAKKKKACisfWfFGk6Bv/ALQmnHlxGeXyLSWfyYxn55PLVtina2C2AdrY+6cU/DXj7wv4vuJ7fQtWjup4EDyRmN422k4yA6gkZwCRnGRnqKAOkooooAKKKKACiiigDw/4v+Ov+EU8Q6xpP9nfav7c8PxW3m+fs8j57pd2Np3f6zOMjp71yH7OP/JQ9Q/7BUn/AKNir2Pxr8JtB8eazDqmqXepQzxW626rayIqlQzNk7kY5y57+lHgr4TaD4D1mbVNLu9Smnlt2t2W6kRlCllbI2opzlB39aAO8ooooAKKKKACiiigAopGZUUsxCqBkknAApaACiiigAooooAKKKKAK9//AMg66/64v/I1Yqvf/wDIOuv+uL/yNWKACiiigAooooAKKKKAK9//AMg66/64v/I1Yqvf/wDIOuv+uL/yNWKACiiigAooooAKKKKAEZVdSrAMpGCCMgiloooAKKKKACiiigAooooA/9k=" id="p2img1"></DIV>


<P class="p36 ft3">Education</P>
<P class="p37 ft5">Bachelor's Degree in Interaction Design, Sterling College, New York</P>
<P class="p38 ft1">2014</P>
<P class="p39 ft8">Excelled in UI/UX coursework.</P>
<P class="p40 ft3">Courses</P>
<P class="p41 ft5">Advanced User Interface Design, Udemy</P>
<P class="p38 ft1">May 2016</P>
</DIV>
</BODY>
</HTML>
