<div class="tab-pane text-center" id="about" role="tabpanel">
    <h5 class="text-muted" id="my_about">{{Auth::user()->about}}</h5>
    <button class="btn btn-success btn-round" id="editModal" data-toggle="modal" data-target="#editAbout"><i class="fa fa-pencil"></i> Edit</button>
</div>