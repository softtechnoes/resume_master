@extends('master1')

@section('title')
    Notes
@endsection

@push('css')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {{-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet"> --}}
    <style>
        /* table{ width:100%;margin-bottom:15px;overflow-x:hidden;-ms-overflow-style:-ms-autohiding-scrollbar;border:1px solid #ddd}.table-responsive>.table{margin-bottom:0} */
    </style>
@endpush

@section('content')
    <div class="container" style="margin-top:7%">
        <div class="row" >    
            <div class="col-md-4"><h3 >Make notes</h3></div>
            {{-- <div class="col-md-2"></div> --}}
            <div class="col-md-8" style="margin-top:5px;">
                <form action="/filter-notes" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-danger pull-right" name="done" value="1"><i class="fa fa-check-square-o"></i> Completed</button>
                    <button type="submit" class="btn btn-success pull-right" name="done" value="0" style="margin-right:5px;"><i class="fa fa-clock-o"></i> Active</button>
                    <button type="submit" name="done" value="2" class="btn btn-info pull-right" style="margin-right:5px;"><i class="fa fa-tasks"></i> All</button><br><br>
                </form>
            </div>      
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box user-table">
                    <div class="">
                        <table class="table" id="notesTable">
                            <thead>
                                <tr>
                                    <th colspan="5">
                                        <div class="note_msg alert alert-danger" style="display:none;">Note required</div>
                                        <div class="row" style="display:flex;">
                                            <div class="col-md-11">
                                                <input type="hidden" name="" id="lat" value="">
                                                <input type="hidden" name="" id="long" value="">
                                                <input type="hidden" id="note_address">
                                                <input type="text" class="form-control" id="note" placeholder="Make a note...">
                                            </div>
                                            <div class="col-md-1" >
                                                {{-- <button class="btn btn-info" onclick="saveNote()" style="width:50px;height:36px;border-radius:0px;border:none;"><i class="fa fa-plus"></i> </button> --}}

                                                <button type="button" class="btn btn-primary btn-round" onclick="saveNote()"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </th>   
                                </tr>
                            </thead>  
                            <tbody>
                                @foreach ($notes as $note)
                                    <tr id="note-{{$note->id}}" >
                                        <td width="5%">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                <input id="check-{{$note->id}}" value="{{$note->id}}" onchange="markActive(this)" type="checkbox" @if($note->done==1) checked @endif>
                                                <span class="form-check-sign"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td width="27%" @if($note->done==1) style="text-decoration: line-through;color:red" @endif ondblclick="editNote(this)" data-value="{{$note->id}}" id="editable-note-{{$note->id}}" note-row="note-{{$note->id}}" address="{{$note->address}}">
                                            {!! $note->note !!}
                                        </td>
                                        <td id="editable-address-{{$note->id}}">
                                            @if (strlen($note->address)>25)
                                                <div class="tooltip_custom">
                                                    {!! str_limit($note->address, 25, $end = '...') !!}
                                                    <span class="tooltiptext">{!! $note->address !!}</span>
                                                </div>
                                            @else
                                                {!! $note->address !!}
                                            @endif
                                        </td>
                                        <?php 
                                        $update_date = new DateTime($note->created_at);
                                        $updated_at = $update_date->format('Y-m-d');
                                        ?>
                                        <td>{{$note->created_at}}</td>
                                        
                                        <td width="20%" >
                                            {{-- <button onclick="deleteNote(this)" value="{{$note->id}} " class="btn btn-danger btn-outline font-16 pull-right"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                            <button onclick="editNoteBtn(this)" cell-id="editable-note-{{$note->id}}" data-value="{{$note->id}}" value="{{$note->note}}" note-row="note-{{$note->id}}" address="{{$note->address}}" class="btn btn-info btn-outline font-16 pull-right"><i class="fa fa-pencil" aria-hidden="true"></i></button> --}}
                                            <button type="button" class="btn btn-danger btn-just-icon pull-right" onclick="deleteNote(this)" value="{{$note->id}} "><i class="fa fa-trash"></i></button>
                                            <button type="button" class="btn btn-info btn-just-icon pull-right" onclick="editNoteBtn(this)" cell-id="editable-note-{{$note->id}}" data-value="{{$note->id}}" value="{{$note->note}}" note-row="note-{{$note->id}}" address="{{$note->address}}"><i class="fa fa-pencil"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div style="padding:15px">
                            <div >*Double tap on the note to edit</div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('notes.partial-script')
@push('js')
<script>
//     if (navigator.geolocation) {
                
//                 navigator.geolocation.getCurrentPosition(showPosition);
              
//             }
// function showPosition(position) {
// var w_lat = position.coords.latitude;
// var w_lng = position.coords.longitude;
// console.log(w_lat);
// }


function saveNote(){
    console.log('done');
    var note = $('#note').val();
    var w_lat= '';
    var w_lng = '';
    var address = '';

    if(note==''){
        $('.note_msg').show();
    }
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });
    $.ajax({
        url: "{{ url('save-note') }}",
        data: { internal_note: note, w_lat: w_lat, w_lng: w_lng, address:address},
        type: 'get',
        dataType: 'json',
        success: function(res){
            if(res=='present'){
                $.growl.notice({ message: "Notes available" });
            }else{
            $('#note').val(''); 
            $('.note_msg').hide();
            var row='<tr id="note-'+res[1][0].id+'"><td><div class="form-check"> <input type="checkbox" id="check-'+res[1][0].id+'" value="'+res[1][0].id+'" onchange="markActive(this)" ><span class="form-check-sign"></span>   </div> </td><td width="35%" ondblclick="editNote(this)" data-value="'+res[1][0].id+'" id="editable-note-'+res[1][0].id+'" note-row="note-'+res[1][0].id+'">'+res[1][0].note+'</td><td>'+res[1][0].address+'</td><td><small>'+res[1][0].created_at+'</small></td> <td ><button onclick="deleteNote(this)" value="'+res[1][0].id+' " class="btn btn-danger btn-just-icon pull-right"><i class="fa fa-trash" aria-hidden="true"></i></button><button onclick="editNoteBtn(this)" cell-id="editable-note-'+res[1][0].id+'" data-value="'+res[1][0].id+'" note-row="note-'+res[1][0].id+'" value="'+res[1][0].note+'" class="btn btn-info btn-just-icon pull-right"><i class="fa fa-pencil" aria-hidden="true"></i></button></td></tr>';
            $('tbody').append(row);
        }
    }
});
}
        // if (navigator.geolocation) {    
        //     var temp = navigator.geolocation.getCurrentPosition(showPosition,showError); 
        // }
        // function showError(error) {
        //     switch(error.code) {
        //         case error.PERMISSION_DENIED:
        //         // console.log("User denied the request for Geolocation.");
        //         var w_lat = $("#lat").val();
        //         var w_lng = $("#long").val();
        //         var note = $('#note').val(); 
                
        //         if(note==''){
        //             $('.note_msg').show();
        //         }
        //         $.get('https://nominatim.openstreetmap.org/reverse?format=json&accept-language=fr&lat='+w_lat+'&lon='+w_lng, function(data){
        //             var str = data.address.postcode;
        //             var pCode = str.replace(/ +/g, "");
        //             var address = '';

        //         $.ajaxSetup({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //                 }
        //         });
        //         $.ajax({
        //             url: "{{ url('save-note') }}",
        //             data: { internal_note: note, w_lat: w_lat, w_lng: w_lng, address:address},
        //             type: 'post',
        //             dataType: 'json',
        //             success: function(res){
        //                 if(res=='present'){
        //                     $.notify('{!!Lang::get('site_lang.note_existing')!!}', {
        //                     className:'warning'
        //                     });
        //                 }else{
        //                 $('#note').val(''); 
        //                 $('.note_msg').hide();
        //                 var row='<tr id="note-'+res[1][0].id+'"><td><div class="checkbox checkbox-info"> <input id="check-'+res[1][0].id+'" type="checkbox" value="'+res[1][0].id+'" onchange="markActive(this)"><label for="check-'+res[1][0].id+'"></label> </div>  </td> <td width="35%" ondblclick="editNote(this)" data-value="'+res[1][0].id+'" id="editable-note-'+res[1][0].id+'" note-row="note-'+res[1][0].id+'">'+res[1][0].note+'</td><td>'+res[1][0].address+'</td><td><small>'+res[1][0].created_at+'</small></td> <td ><button onclick="editNoteBtn(this)" cell-id="editable-note-'+res[1][0].id+'" data-value="'+res[1][0].id+'" note-row="note-'+res[1][0].id+'" value="'+res[1][0].note+'" class="btn btn-info btn-outline font-16"><i class="fa fa-pencil" aria-hidden="true"></i></button><button onclick="deleteNote(this)" value="'+res[1][0].id+' " class="btn btn-danger btn-outline font-16"><i class="fa fa-trash" aria-hidden="true"></i></button>             </td></tr>';
        //                 $('tbody').append(row);
        //                 }
                        
        //             }
        //         });
        //         });
        //         break;
        //         case error.POSITION_UNAVAILABLE:
        //         console.log("Location information is unavailable.");
        //         break;
        //         case error.TIMEOUT:
        //         console.log("The request to get user location timed out.");
        //         break;
        //         case error.UNKNOWN_ERROR:
        //         console.log("An unknown error occurred.");
        //         break;
                
        //     }
        // }
    //     function showPosition(position) {
    //     var w_lat = position.coords.latitude;
    //     var w_lng = position.coords.longitude;
    //     var note = $('#note').val(); 
    //     if(note==''){
    //         $('.note_msg').show();
    //     }
    //     $.get('https://nominatim.openstreetmap.org/reverse?format=json&accept-language=fr&lat='+w_lat+'&lon='+w_lng, function(data){
    //         var str = data.address.postcode;
    //         var pCode = str.replace(/ +/g, "");
    //         var address = data.address;
    //         address = address.house_number+' '+address.road+' '+address.town+' '+pCode;

    //         $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //                 }
    //         });
    //         $.ajax({
    //             url: "{{ url('save-note') }}",
    //             data: { internal_note: note, w_lat: w_lat, w_lng: w_lng, address:address},
    //             type: 'post',
    //             dataType: 'json',
    //             success: function(res){
    //                 if(res=='present'){
    //                     $.notify('{!!Lang::get('site_lang.note_existing')!!}', {
    //                     className:'warning'
    //                     });
    //                 }else{
    //                 $('#note').val(''); 
    //                 $('.note_msg').hide();
    //                 var row='<tr id="note-'+res[1][0].id+'"><td><div class="checkbox checkbox-info"> <input id="check-'+res[1][0].id+'" type="checkbox" value="'+res[1][0].id+'" onchange="markActive(this)"><label for="check-'+res[1][0].id+'"></label></div></td> <td width="35%" ondblclick="editNote(this)" data-value="'+res[1][0].id+'" id="editable-note-'+res[1][0].id+'" note-row="note-'+res[1][0].id+'">'+res[1][0].note+'</td><td>'+res[1][0].address+'</td><td><small>'+res[1][0].created_at+'</small></td> <td ><button onclick="editNoteBtn(this)" cell-id="editable-note-'+res[1][0].id+'" data-value="'+res[1][0].id+'" note-row="note-'+res[1][0].id+'" value="'+res[1][0].note+'" class="btn btn-info btn-outline font-16"><i class="fa fa-pencil" aria-hidden="true"></i></button><button onclick="deleteNote(this)" value="'+res[1][0].id+' " class="btn btn-danger btn-outline font-16"><i class="fa fa-trash" aria-hidden="true"></i></button></td></tr>';
    //                 $('tbody').append(row);
    //             }
    //         });
    //     });
    //     }
        
    // }
    function deleteNote(value){
        var id = $(value).val();
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
            url: "{{ url('delete-note') }}"+'/'+id,
            data: { 'id': id },
            type: 'get',
            dataType: 'json',
            success: function(res){
                $("#note-"+res).remove();
                // $.notify('{!!Lang::get('site_lang.note_deleted')!!}', {
                //     className:'warning'
                // });
                $.growl.notice({ message: "Done !" });
                
            }
        });
    }
</script>
@endpush
@include('notes.partial-script')