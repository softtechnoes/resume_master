<h3>Hi, {{ $name }}</h3>
<p>Activate your account.<br> Your confirmation code is <strong>{{ $code }}</strong></p>
You can verify your email from
<a href="{{url('user/verify', $token)}}">Verify Email</a>