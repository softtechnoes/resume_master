@push('css')
  <style>
    .modal { overflow-y: auto }
  </style>
@endpush 
 <!-- Edit About -->
   <div class="modal fade" id="editAbout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
             <h5 class="modal-title text-center" id="exampleModalLabel">Edit About</h5>
           </div>
           <div class="modal-body">
             <div class="row">
               <div class="col-md-4">About</div>
               <div class="col-md-8"> 
                 <div class="input-group">
                   <textarea name="" id="user_about" cols="60" rows="10"></textarea>
                 </div>
               </div>
             </div><br>
           </div>
           <div class="modal-footer">
             <div class="left-side">
               <button type="button" id="updateAbout" class="btn btn-default btn-link" data-dismiss="modal">Update</button>
             </div>
             <div class="divider"></div>
             <div class="right-side">
               <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Cancel</button>
             </div>
           </div>
         </div>
     </div>
   </div>
 
 <div class="modal fade" id="addDiploma" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title text-center" id="exampleModalLabel">Add Diploma</h5>
      </div>
      <div class="modal-body"> 
        <div class="row">
          <div class="col-md-4">About</div>
          <div class="col-md-8"> 
            <div class="input-group">
              <input type="text" placeholder="Course Name" id="diploma_course_name" class="form-control" >
              <div class="input-group-append">
                <span class="input-group-text"><i class="fa fa-book"></i></span>
              </div>
            </div>
          </div>
        </div><br>
       
      </div>
      <div class="modal-footer">
        <div class="left-side">
          <button type="button" id="saveAbout" class="btn btn-default btn-link" data-dismiss="modal">Save</button>
        </div>
        <div class="divider"></div>
        <div class="right-side">
          <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
</div>
</div>
 
 @push('scripts')
 <script>
 var diploma_percentage_old = $('#diploma_percentage').val();
//  console.log(diploma_percentage_old);
  $('#diploma_percentage').change(function(){
    var diploma_percentage = $('#diploma_percentage').val();
    if(diploma_percentage>100){
      $("#warning").modal('show');
      $('#diploma_percentage').val(diploma_percentage_old);
    }
  });
  </script>
 @endpush