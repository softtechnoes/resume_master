@extends('master1')

@section('title')
Generated QR Code
@endsection

@section('content')
    <div class="container" style="margin-top:10%">
        
        <div class="row"></div>
            <div class="col-md-6"><h2>Generated QR Code</h2>{!! $code !!}</div>
        </div>
        
    </div>
@endsection