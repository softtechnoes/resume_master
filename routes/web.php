<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!  
|
*/

Route::get('/', 'AttendanceController@index1');

Auth::routes(['verify' => true]);
Route::get('/logout', function(){
    Auth::logout();
    return Redirect::to('login');
 });

 /**
 * Password Reset Route(S)
 */
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

/**
 * Email Verification Route(s)
 */
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');




 Route::get('/verify','HomeController@verify');
 Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
 Route::get('/edit-profile','UsersController@edit');
 Route::get('/update-profile','UsersController@update');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('comments','HomeController@comments');
Route::get('comment_back','HomeController@comment_back');

// Socialite Routes
Route::get('login/github', 'Auth\LoginController@redirectToProvider');
Route::get('login/github/callback', 'Auth\LoginController@handleProviderCallback');  
Route::get('add-about', 'HomeController@addAbout');    
Route::get('add-high-school', 'HomeController@addHighSchool');
Route::get('add-intermediate', 'HomeController@addIntermediate');
Route::get('add-graduation', 'HomeController@addGraduation');
Route::get('add-master', 'HomeController@addMaster');
Route::get('add-diploma', 'HomeController@addDiploma');
Route::get('add-experience', 'ExperienceController@addExperience');
Route::get('add-skills', 'HomeController@addSkills');
Route::get('update-about', 'HomeController@updateAbout');

// Attendance
Route::get('load-attandance', 'AttendanceController@index');
Route::get('make-attandance', 'AttendanceController@create');


Route::resource('tasks', 'AttendanceController');
Route::get('view-resume', 'HomeController@resumeOutput');

// Notes
Route::get('notes', 'NotesController@index');
Route::get('save-note', 'NotesController@storeNote');
Route::get('delete-note/{id}', 'NotesController@delete'); 
Route::get('update-note/{id}', 'NotesController@update');
Route::get('change-status/{id}', 'NotesController@changeStatus');
Route::get('change-status-undone/{id}', 'NotesController@changeStatusUndone');
Route::post('filter-notes', 'NotesController@filter'); 

//QR scanner
Route::get('qr-scanner', 'QrController@index');
Route::get('qr-code', function () {
    return QrCode::size(500)->generate('Welcome to kerneldev.com!');
});
Route::post('generate-qr-scanner', 'QrController@generate');
Route::get('scanner', 'QrController@scan');


Route::post('send-query', 'UsersController@query'); 


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

