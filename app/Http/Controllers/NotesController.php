<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;
use Illuminate\Support\Facades\Auth;
use App\Models\Settings\SystemSettings;

class NotesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $notes = Note::where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get();
        // dd($notes);
        // $system_setting  = SystemSettings::first();
        // $latitude        = $system_setting['fallback_lat'];
        // $longitude       = $system_setting['fallback_lng'];
        return view('notes.notes',compact('notes'));
    }
    public function storeNote(Request $request){  
        $requestData['note']    = $request->internal_note;
        $requestData['user_id'] = Auth::user()->id;
        $requestData['lat'] = $request->w_lat;
        $requestData['lng'] = $request->w_lng;

        if(Note::where('note', '=', $request->internal_note)->exists()){
            $error="present";
            return response()->json($error);
        }
        else{
        Note::create($requestData); 
        $id=Note::where('note',$requestData['note'])->get()->toArray();
        return response()->json([$requestData,$id]);
        }
    }
    public function delete($id){
        Note::where('id',$id)->delete();
        return response()->json($id);
    }

    public function update(Request $request, $id){
        Note::where('id',$id)->update(['note'=>$request->note]);
        return response()->json([$id,$request->note,$request->data_id]);
    }

    public function changeStatus($id){
        $test = Note::where('id',$id)->update(['done'=>1]);
        return response()->json($id);
    } 

    public function changeStatusUndone($id){
        Note::where('id',$id)->update(['done'=>0]);
        return response()->json($id);
    }
    public function filter(Request $request){
      
        $note=$request->done;
        $system_setting  = SystemSettings::first();
        $latitude        = $system_setting['fallback_lat'];
        $longitude       = $system_setting['fallback_lng'];
        if($note==2){
            $notes = Note::where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get();
            return view('notes.notes',compact('notes','latitude','longitude'));
        }
        else{
            $notes=Note::where('user_id',Auth::user()->id)->where('done',$note)->orderBy('created_at','desc')->get();
            return view('notes.notes',compact('notes','latitude','longitude'));
        }
        //return view('notes.notes',compact('notes','latitude'));
        
    }
}
