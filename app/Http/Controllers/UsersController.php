<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Queries;

class UsersController extends Controller
{
    public function edit(){
        $id=Auth::user()->id;
        $user=User::where('id',$id)->get();
        // dd($user);
        return view('edit_profile',compact('user'));
    }

    public function update(Request $request){
        $id=Auth::user()->id;
        // dd($request->all());
        $name              = $request->name;
        $father_name       = $request->father_name;
        $mobile            = $request->mobile;
        $facebook_link     = $request->facebook_link;
        $instagram_link    = $request->instagram_link;
        $linkedin_url      = $request->linkedin_link;
        $twitter_url       = $request->twitter_link;
        $website           = $request->website;
        $age               = $request->age;
        $job               = $request->job;
        $about             = $request->about;

        $user=User::where('id',$id)->update([
                'name'=>$name,
                'father_name'=>$father_name,
                'mobile'=>$mobile,
                'facebook_url'=>$facebook_link,
                'instagram_url'=>$instagram_link,
                'linkedin_url'=>$linkedin_url,
                'twitter_url'=>$twitter_url,
                'website'=>$website,
                'age'=>$age,
                'job'=>$job,
                'about'=>$about,
                ]);

        return redirect()->back()->with(['success' => 'Profile Updated Successfully']);
    }
    public function query(Request $request){
        
        $requestData['name'] = $request->name;
        $requestData['email'] = $request->email;
        $requestData['message'] = $request->message;
        $requestData['ip'] ='';
        Queries::create($requestData); 
        return back()->with('success', 'Your message has been sent suucessfully.');
    }
}
