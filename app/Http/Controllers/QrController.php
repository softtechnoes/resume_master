<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QrController extends Controller
{
    public function index(){
        return view('qr_scanner.qr_scanner');
    }
    public function generate(Request $request){
        // dd($request->all());
        $info = $request->info;
        $code = QrCode::size(400)->generate($info);
        return view('qr_scanner.qr_generated',compact('code'));
    }

    public function scan(){
        return view('qr_scanner.scanner');
    }
}
